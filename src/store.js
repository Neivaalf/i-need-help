import { readable } from "svelte/store";

const theme = readable("dark");

export { theme };
